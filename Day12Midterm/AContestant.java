/**
 * This class is the abstract clss which contestants extend.
 * 
 * @author Alec Bokowski
 *
 */
public abstract class AContestant implements IContestant {
	
	private String name;
	
	/**
	 * This method sets up your contestant.
	 * 
	 * @param inName the name of the contestant.
	 */
	public AContestant(String inName) {
		this.name = inName;
	}
	
	/**
	 * @return The name of the contestant.
	 */
	@Override
	public String toString() {
		return this.name;
	}

}
