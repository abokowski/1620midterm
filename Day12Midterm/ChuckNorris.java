/**
 * This class represents Chuck Norris.
 * 
 * @author Alec Bokowski
 *
 */
public class ChuckNorris extends AContestant {
	
	/**
	 * Constructor for Chuck Norris.
	 */
	public ChuckNorris() {
		super("Chuck Norris");
	}

	/**
	 * this method handles Chuck Norris in his super intense one on ones.
	 * 
	 * @param contestant The contestant that chuck norris is facing.
	 * 
	 * @return returns the outcome of the one on one.
	 */
	@Override
	public String defendAgainst(IContestant contestant) {
		if (contestant instanceof MichelangeloTheNinjaTurtle) {
			return HelperStrings.ChuckDefendingMichelangelo;
		} else if (contestant instanceof DarthVader) {
			return HelperStrings.ChuckDefendingDarthVader;
		} else {
			return "uh oh";
		}
	}


}
