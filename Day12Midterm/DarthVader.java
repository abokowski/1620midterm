/**
 * This class represents Darth Vader.
 * 
 * @author Alec Bokowski
 *
 */
public class DarthVader extends AContestant {
	
	/**
	 * This is the constructor for Darth Vader.
	 */
	public DarthVader() {
		super("Darth Vader");
	}
	
	/**
	 * This method handles Darth Vaders crazy light saver wielding one on ones.
	 * 
	 * @param contestant the contestant that Darth Vader is facing.
	 * 
	 * @return the outcome of the one on one.
	 */
	@Override
	public String defendAgainst(IContestant contestant) {
		if (contestant instanceof MichelangeloTheNinjaTurtle) {
			return HelperStrings.DarthVaderDefendingMichelangelo;
		} else if (contestant instanceof ChuckNorris) {
			return HelperStrings.DarthVaderDefendingChuckNorris;
		} else {
			return "uh oh";
		}
	}
}
