/**
 * This class represents Michaelangelo
 * 
 * @author Alec Bokowski
 *
 */
public class MichelangeloTheNinjaTurtle extends AContestant {
	
	/**
	 * This is the constructor for Michaelangelo
	 */
	public MichelangeloTheNinjaTurtle() {
		super("Michelangelo");
	}

	/**
	 * This method handles Michaelangelos one on ones.
	 * 
	 * @param contestant the contestant that Michaelangelo is facing.
	 * @return the outcome of the one on one.
	 */
	@Override
	public String defendAgainst(IContestant contestant) {
		if (contestant instanceof ChuckNorris) {
			return HelperStrings.MichelangeloDefendingChuck;
		} else if (contestant instanceof DarthVader) {
			return HelperStrings.MichelangeloDefendingDarthVader;
		} else {
			return "uh oh";
		}
	}
	
	
}
