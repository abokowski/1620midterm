/**
 * This interface is the base of a contestant, and represents what any contestant needs if they are to one on one.
 * 
 * @author Alec Bokowski
 *
 */
public interface IContestant {
	
	/**
	 * 
	 * @param contestant the contestant that the player is facing.
	 * @return the outcome of the one on one.
	 */
	String defendAgainst(IContestant contestant);
	
}
